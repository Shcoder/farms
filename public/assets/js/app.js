var animalFarms = angular.module('animalFarms', [], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
})

function AnimalCtrl($scope, $http){
  $scope.animals = [];
  $scope.farms = [];
  $scope.stats = {
      kill: 0
    , live: 0
  }

  $scope.cmdRun = function() {
    $scope.cmd = $scope.cmd || "";
    var comand = $scope.cmd.replace(/\s+/g," ").toLowerCase()
      , indexAnimal = 0
      , indexFarm = 0;

    comand = comand.split(" ");

    if(!!~comand.indexOf('убить')){
      indexAnimal = parseInt(comand[1].replace(/\D+/g,""))-1;
      $scope.deleteAnimalOne(indexAnimal);
    }else if(!!~comand.indexOf('переместить')){
      indexAnimal = parseInt(comand[0].replace(/\D+/g,""))-1;
      indexFarm = parseInt(comand[2].replace(/\D+/g,""))-1;
      $scope.transfer(indexAnimal, indexFarm);
    }
  }

  $scope.getAnimals = function() {
    $http.get('/animal').success(function(res) {
      if(!!res){
        $scope.animals = JSON.parse(res.animals);
        $scope.farms = JSON.parse(res.farms);
        $scope.stats.live = $scope.animals.length;

        $.jGrowl("Новый день");
      }else{
        $.jGrowl("Тут текст ошибки, я не придумал");
      }

    }) 
  }

  $scope.deleteAnimalOne = function(index) {
    var id = $scope.animals[index].id
      , animal = $scope.animals[index].title_ru
      , dropAnimal = $scope.animals.splice(index, 1);

    $scope.stats.kill++;
    $scope.stats.live--;

    $http.delete('/animal/'+id).success(function(res) {
      if(!!res){
        $.jGrowl("НЯМ-НЯМ");
      }else{
        $scope.animals.push(dropAnimal);
        $scope.stats.kill--;
        $scope.stats.live++;
        $.jGrowl(animal+" ударило вас коленом под дых и убежало, назад на ферму");
      }
    })
  }

  $scope.deleteAnimalAll = function() {
    $scope.stats.kill += $scope.animals.length;
    $scope.animals = [];
    $scope.stats.live = 0;

    $http.delete('/animal').success(function(res) {
      if(!!res){
        $.jGrowl("Сделано!");
      }else{
        $.jGrowl("Задание провалено, но ферму не вернуть");
      }
    })
  }

  $scope.transfer = function(indexAnimal, indexFarm) {
    var aid = $scope.animals[indexAnimal].id
      , fid = $scope.farms[indexFarm].id;

    $scope.animals[indexAnimal].farm_id = fid;
    
    $http.get('/animal/transfer/'+aid+'/'+fid).success(function(res) {
      if(!!res){
        $.jGrowl("Перегонка завершена");
      }else{
        $.jGrowl("Стадо тупит");
      }
    })
  }

  ;(function init(){
    $scope.getAnimals();
    setInterval(function(){$scope.getAnimals();},20000);
  }())

}