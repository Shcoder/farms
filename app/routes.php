<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('before' => 'detectLang','uses' => 'SiteController@getIndex'));
Route::get('/animal', 'AnimalController@getIndex');

Route::delete('/animal', 'AnimalController@destroyAll');
Route::delete('/animal/{id}', 'AnimalController@destroyOne')->where('id', '[0-9]+');

Route::get('/animal/transfer/{aid}/{fid}', 'AnimalController@transfer')
  ->where(array("aid"=>"[0-9]+", "fid"=>"[0-9]+"));
