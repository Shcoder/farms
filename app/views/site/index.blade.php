@extends('site.layouts.default')

{{-- Content --}}
@section('content')

<div class="container" ng-controller="AnimalCtrl">
  <div class="well">
    <div class="row">
    <span class="help-block">Пример: убить корову#1, корову#1 переместить ферму#2</span>
      <input ng-model="cmd" type="text" class="form-control">
      <button ng-click="cmdRun()" class="btn col-md-12 btn-success btn-lg">Выполнить команду</button>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="row js-group-btn-center">
          <div class="btn-group">
            <button class="btn btn-info" ng-click="getAnimals()">Обновить</button>
            <button class="btn btn-danger" ng-click="deleteAnimalAll()">Изничтожить</button>
          </div>
        </div>
        <h3>Статистика:</h3>
        <table class="table table-bordered js-table-stats table-striped">
          <thead>
            <tr>
              <th class="col-md-6">Убито</th>
              <th class="col-md-6">Пока ещё живут</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>[[stats.kill]]</td>
              <td>[[stats.live]]</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <h1>Фермы:</h1>
  <div class="row">
    <div class="col-md-3" ng-repeat="(key,farm) in farms track by $index">
      <h3>[[farm.title_ru]]</h3>
      <table class="table">
        <tbody>
          <tr ng-repeat="animal in animals track by $index" ng-if="(farm.id==animal.farm_id)">
            <td class="col-md-11">[[animal.title_ru]] #[[$index+1]]</td>
            <td>
              <button ng-click="deleteAnimalOne($index)" href="#" class="btn btn-danger btn"><i class="glyphicon glyphicon-cutlery"></i></button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

@stop