<?php
class FarmTableSeeder extends Seeder {

    public function run()
    {
        DB::table('farms')->delete();
        Farm::create(array(
          'id' => 1,
          'title_ru' => 'Ферма #1',
          'title_en' => 'Farm #1',
          'created_at' => new DateTime,
          'updated_at' => new DateTime
        ));

        Farm::create(array(
          'id' => 2,
          'title_ru' => 'Ферма #2',
          'title_en' => 'Farm #2',
          'created_at' => new DateTime,
          'updated_at' => new DateTime
        ));

        Farm::create(array(
          'id' => 3,
          'title_ru' => 'Ферма #3',
          'title_en' => 'Farm #3',
          'created_at' => new DateTime,
          'updated_at' => new DateTime
        ));

        Farm::create(array(
          'id' => 4,
          'title_ru' => 'Ферма #4',
          'title_en' => 'Farm #4',
          'created_at' => new DateTime,
          'updated_at' => new DateTime
        ));
 
    }
}