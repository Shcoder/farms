<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('animals', function($t) {
        $t->increments('id')->unsigned();

        $t->string('title_ru');
        $t->string('title_en');
        $t->integer('farm_id');
        $t->boolean('is_delete')->default(0);
        $t->boolean('status')->default(1);

        $t->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('animals');
	}

}
