<?php

class AnimalController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$animals_ru = array('Гуманоид', 'Черепаха', 'Обезьяна', 'Корова', 'Тигр', 'Свинья');
		$animals_en = array('Humanoid', 'Turtle', 'monkey', 'cow', 'Tiger', 'Pig');

		//inc refresh count
		$refreshCount = (int)Session::get('refresh_count');
		$refreshCount++;
		Session::put('refresh_count', $refreshCount);

		// find where one animal at farm
		$farmsOneAnimal = DB::table('animals')
			->select("farm_id", DB::raw('count(*) as `cnt`'))
			->having('cnt','=','1')
			->groupBy('farm_id')
			->lists('farm_id');


		$farms = Farm::all()->sortBy('id');
		// $farms = Farm::whereNotIn('id', $farmsOneAnimal)->get();
		foreach ($farms as $farm) {
			if(in_array($farm->id, $farmsOneAnimal)){
				continue;
			}

			$randAnimal = rand(0, 9);
			for($i=0; $i<$randAnimal; $i++){
        $keyRand = array_rand($animals_ru);
        $animal = new Animal;
        $animal->title_ru = $animals_ru[$keyRand];
        $animal->title_en = $animals_en[$keyRand];
        $animal->farm_id = $farm->id;
        $animal->created_at = new DateTime;
        $animal->updated_at = new DateTime;
        $animal->save();
			}
		}

		$animals = Animal::all()->sortBy('id')->toJson();
		$farms = $farms->toJson();
		return compact('animals', 'farms');
	}

	public function destroyOne($id)
	{
		return Animal::destroy((int)$id);

	}

	public function destroyAll()
	{
		return DB::table('animals')->delete();
	}

	public function transfer($aid, $fid){
		$animal = Animal::find((int)$aid);
		$animal->farm_id = (int)$fid;
		return $animal->save();
	}

}